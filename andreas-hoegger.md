# Leb wohl Andy

Die tiefe freundschaftliche Art von Andy, sowie sein Ehrgeiz steckten neben Kollegen in der Eclipse Community auch die Kollegen bei BSI und Kunden  an.

Andys Spitzendisziplin war die Software, das Programmieren.
Zum Vergleich: Als Kinder spielen wir mit Spielzeugen, machen coole Sachen und auch ein paar unsinnige Sachen. 
Und wenn ein Kind etwas cooles hat, dann wollen es die anderen Kinder auch haben.
Wenn wir grösser werden, dann werden wir zwar erwachsen, aber dieses Verlangen bleibt bestehen. 
Die Spielsachen werden dann zu Werkzeugen und die coolen Sachen - die andere wollen - werden zu Produkten die man verkaufen kann.
Andy war seit Beginn bei der BSI die treibende Kraft für unser wichtigstes Werkzeug.

Wie bei den coolen Sachen der Kinder hat sich Andy dafür stark gemacht, dass auch alle anderen mit diesem Spielzeug spielen können. 
Er hat sich immer dafür eingesetzt und auch dafür gekämpft, dass diese wichtigen Grundlagen-Tools und diese Plattform nicht einfach ein
firmeninternes Schattendasein fristen, sondern allen Entwicklern weltweit zur Verfügung stehen sollen. 
Damit nicht jeder Programmierer wieder bei Null anfangen muss. 
Dieses Projekt nannten wir Eclipse Scout. Scout wird auch heute breit weiterentwickelt und immer reicher an Funktionen und Möglichkeiten. 
Dieser Eclipse Scout ist die Basis Technologie vieler Unternehmen geworden, auch der BSI.

Das alles war aber kein Spaziergang, viele Abende und Wochenenden mit Fehler suchen, Debugging und Tuning.
Für Andy war das Arbeiten jedoch nie eine Pflicht, er hatte Freude und Spass daran, es war auch sein Hobby.

Andy war ein ausgesprochen optimistischer, toleranter und fröhlicher Familienmensch.
Wenn immer möglich, hat er Zeit mit seiner Familie verbracht. Er war sportlich, fit, aktiv. 
Mit seiner Frau zusammen hat er sich viele Träume verwirklicht und das Leben auch genossen. Er hat so vieles einfach richtig gemacht.

Sein Sohn und seine kleine Tochter wachsen nun ohne ihren Vater auf. Sie werden später erkennen, was für ein grosser Geist 
in Andy wirkte, was er aufbaute und mitgestaltete.

In Liebe, Deine Arbeitskollegen, Deine BSI, Deine Kunden

# Farewell Andy


The deep, friendly nature of Andy and his ambitious character immediately infected colleagues in teh Eclipse Community as well as colleagues at BSI and customer site.

Andy's top discipline was software, programming.
For comparison: As children we play with toys, do cool things and also some nonsense things.
And when one has something cool, the other wants it too.
As we grow up, we mature, but that primary desire remains.
The former toys turn to tools and the cool things - that others want too - turn to products that are sold.
Andy has been the driving force behind our most important tool at BSI since our start.

As with the cool things the kids do, Andy has made a point, ensuring that everyone else can play with these toys too.
He has always advocated and also fought to ensure that these important basic tools and this great platform are not simply stored away in a 
company's storage space, but stand free to all developers worldwide.
So that no programmer has to start from scratch.
We called this project Eclipse Scout. Scout is still being developed on a broad basis today and is becoming increasingly rich in functions and options.
This Eclipse Scout has become the basic technology of many companies meanwhile, including BSI itself.

But all of this was no walk in the park, so many evenings and weekends spent looking for errors, debugging and tuning.
For Andy, however, work was never a duty, he enjoyed it and had fun, it was also his hobby.

Andy was a very optimistic, tolerant and happy family man.
Whenever possible, he spent time with his family. He was sporty, fit, active.
Together with his wife he realized many dreams and enjoyed life and sun. He just got so many things right.

His son and little daughter are now growing up without their father. In time they will realize the great spirit
that drove Andy while creating and succeeding.

With love, your work colleagues, your BSI, your customers


Please add your contribution below via a [merge request] (https://gitlab.eclipse.org/eclipsefdn/memorials/-/merge_requests)

## Ivan Motsch

wir haben zusammen programmiert, gestritten, gefeiert und lange Diskussionen geführt. Leben pur. Ich behalte genau das in Erinnerung.

